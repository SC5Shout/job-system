# FJS

FJS is a small Job System for parallel programming

### How to clone
git clone https://gitlab.com/SC5Shout/job-system.git

### How to build
FJS uses [premake](https://premake.github.io/) to support cross-platform building (but it needs few changes to run not only on Windows)

Everything you need is to run `Win-Gen-Premake.bat` script

It can also be built with CMake. Run the `Win-Gen-Cmake.bat` script or open the working directory in your favourite editor that supports CMake.
It's needed to have CMake installed and added to path environmental variables in order to run the script.

### How to use
Initialization
```cpp
	//create an JobSystem object and declare how many workers you need
	JobSystem js{ 4 };

	//add the main thread to the job system
	//since it is the main thread, you don't need to remove/pop it, because it will be done for you when the application closes
	js.PushCurrentThread();
```

Job creation - all methods must be called from the thread that is in a JobSystem's thread pool (use JobSystem::PushCurrentThread() to add a thread into the pool)
```cpp
	auto job = js.CreateJob([](void*, JobSystem&, Job*){
		DoWork();
	});
	js.RunAndWait(job);
```

Job has its (limited) storage that you can use - *FAST*
```cpp
	int x = 0;
	auto job = js.CreateJobFunctor([&x](JobSystem& js, Job* root){
		VeryExpensiveCalculations(x);
	});
	js.RunAndWait(job);
```

Job can also handle any size objects using the heap, but it is less efficient, because it uses std::function internally
```cpp
        //you don't need to take a (JobSystem&, Job*) as parameter
	auto job = js.CreateJob(nullptr, [](){});
	js.RunAndWait(job);
```

Job System allow you to create a job that is parented to another job
```cpp
        //remember to protect io stream, the example below does not do that
        auto root = js.CreateJob([](void*, JobSystem&, Job*){ std::cout << "Parent job\n"; });

        //let root run
	js.RunAndRetain(root);

	auto child1 = js.CreateJob([](void*, JobSystem&, Job*) {
		std::this_thread::sleep_for(5s);
		std::cout << "child1 job\n";
	}, root);
	js.Run(child1);

	auto child2 = js.CreateJob([](void*, JobSystem&, Job*) {
		std::this_thread::sleep_for(1s);
		std::cout << "child2 job\n";
	}, root);
	js.Run(child2);

	auto child3 = js.CreateJob([](void*, JobSystem&, Job*) {
		std::this_thread::sleep_for(3s);
		std::cout << "child3 job\n";
	}, root);
	js.Run(child3);

	js.WaitAndRelease(root);
```

You can also create a job/parented job inside a root job function

```cpp
	auto root = js.CreateJob([](void*, JobSystem& js, Job* root) {
		std::cout << "root job\n";

		auto child1 = js.CreateJob([](void*, JobSystem&, Job*) {
			std::this_thread::sleep_for(5s);
			std::cout << "child1 job\n";
		}, root);
		js.Run(child1);

		auto child2 = js.CreateJob([](void*, JobSystem&, Job*) {
			std::this_thread::sleep_for(1s);
			std::cout << "child2 job\n";
		}, root);
		js.Run(child2);

		auto child3 = js.CreateJob([](void*, JobSystem&, Job*) {
			std::this_thread::sleep_for(3s);
			std::cout << "child3 job\n";
		}, root);
		js.Run(child3);
	});
	js.RunAndWait(root);
```

### Main features to come:
 - more parallel algorithms
 - benchmarks
