#pragma once

#include "Fjs_JobSystem.h"

#include <ranges>
#include <latch>
//#include <xutility>

namespace Fjs {
	namespace stdr = std::ranges;

	struct Config
	{
		static constexpr uint32_t OVERSUBSCRIPTION_MULTIPLIER = 32;
		static constexpr uint32_t OVERSUBMISSION_MULTIPLIER = 4;
	};

	struct StaticPartitionKey
	{
		uint32_t chunkNumber = 0;
		uint32_t startAt = 0;
		uint32_t size = 0;

		[[nodiscard]] inline operator bool() const
		{
			return chunkNumber != static_cast<uint32_t>(-1);
		}
	};

	template <typename Iterator>
	struct IteratorRange
	{
		Iterator first;
		Iterator last;
	};

	struct ForEachPartitonData
	{
		ForEachPartitonData(const uint32_t minCountPerThread, const uint32_t count)
			: chunks(std::min(minCountPerThread, count)),
			chunkSize(count / chunks),
			unchunkedItems(count% chunks)
		{
		}

		[[nodiscard]] StaticPartitionKey getChunkKey(const uint32_t i) const
		{
			uint32_t thisChunkSize = chunkSize;
			uint32_t begin = i * thisChunkSize;
			if (i < unchunkedItems) {
				begin += i;
				++thisChunkSize;
			} else begin += unchunkedItems;

			return { i, begin, thisChunkSize };
		};

		[[nodiscard]] StaticPartitionKey getNextKey()
		{
			const auto i = consumedChunks++;
			if (i < chunks) {
				return getChunkKey(i);
			} return StaticPartitionKey{ static_cast<uint32_t>(-1), 0, 0 };
		};

		std::atomic<uint32_t> consumedChunks{ 0 };
		const uint32_t chunks = 0;
		const uint32_t chunkSize = 0;
		const uint32_t unchunkedItems = 0;
	};

	template <std::integral Iterator, std::invocable<Iterator> FuncT>
	void for_each(JobSystem& js, Iterator first, Iterator last, FuncT func)
	{
		static const auto for_loop = [](Iterator first, Iterator last, FuncT func) {
			for (auto it = first; it != last; ++it) {
				func(it);
			}
		};

		const uint32_t hwThreads = std::thread::hardware_concurrency();
		const uint32_t minCountPerThread = hwThreads * Config::OVERSUBSCRIPTION_MULTIPLIER;
		if (hwThreads > 1) {
			const uint32_t count = last - first;
			if (count >= minCountPerThread) {
				ForEachPartitonData jobData(minCountPerThread, count);

				const auto getChunk = [first](const StaticPartitionKey key) {
					const auto _First = first + (Iterator)key.startAt;
					return IteratorRange<Iterator>{ _First, _First + (Iterator)key.size };
				};

				std::latch l(hwThreads * Config::OVERSUBMISSION_MULTIPLIER);

				for (uint32_t i = 0; i < hwThreads * Config::OVERSUBMISSION_MULTIPLIER; ++i) {
					auto job = js.CreateJob(nullptr, [func, &getChunk, &jobData, &l]() {
						while (auto key = jobData.getNextKey()) {
							const auto chunk = getChunk(key);
							for_loop(chunk.first, chunk.last, func);
						}
						l.count_down();
					});

					js.Run(job);
				}
				l.wait();
			} else for_loop(first, last, func);
		} else for_loop(first, last, func);
	}

	template <std::random_access_iterator Iterator, typename FuncT>
	void for_each(JobSystem& js, Iterator first, Iterator last, FuncT func)
	{
		static const auto for_loop = [](Iterator first, Iterator last, FuncT func) {
			for (auto it = first; it != last; ++it) {
				func(*it);
			}
		};

		const uint32_t hwThreads = std::thread::hardware_concurrency();
		const uint32_t minCountPerThread = hwThreads * Config::OVERSUBMISSION_MULTIPLIER;
		if (hwThreads > 1) {
			const uint32_t count = std::distance(first, last);
			if (count >= minCountPerThread) {
				ForEachPartitonData jobData(minCountPerThread, count);

				const auto getChunk = [first](const StaticPartitionKey key) {
					const auto _First = first + key.startAt;
					return IteratorRange<Iterator>{ _First, _First + key.size };
				};

				std::latch l(hwThreads * Config::OVERSUBMISSION_MULTIPLIER);

				for (uint32_t i = 0; i < hwThreads * Config::OVERSUBMISSION_MULTIPLIER; ++i) {
					auto job = js.CreateJob(nullptr, [func, &getChunk, &jobData, &l]() {
						while (auto key = jobData.getNextKey()) {
							const auto chunk = getChunk(key);
							for_loop(chunk.first, chunk.last, func);
						}
						l.count_down();
					});

					js.Run(job);
				}

				l.wait();

			} else for_loop(first, last, func);
		} else for_loop(first, last, func);
	}

	template<stdr::random_access_range Range, typename FuncT>
	void for_each(JobSystem& js, Range&& range, FuncT&& func)
	{
		for_each(js, stdr::begin(range), stdr::end(range), std::move(func));
	}
}