#pragma once

#include <atomic>
#include <array>
#include <cassert>
#include <exception>

namespace Fjs {
	template<typename T, size_t SIZE>
	struct WorkStealingQueue
	{
		static_assert(!(SIZE& (SIZE - 1)), "SIZE must be a power of two");
		static constexpr size_t MASK = SIZE - 1;

		inline void push(const T& item)
		{
			int64_t oldBottom = bottom.load(std::memory_order_relaxed);
			items[oldBottom & MASK] = item;
			bottom.store(oldBottom + 1, std::memory_order_release);
		}

		inline void push(T&& item)
		{
			int64_t oldBottom = bottom.load(std::memory_order_relaxed);
			items[oldBottom & MASK] = std::move(item);
			bottom.store(bottom + 1, std::memory_order_release);
		}

		[[nodiscard]] inline T pop()
		{
			int64_t newBottom = bottom.fetch_sub(1, std::memory_order_seq_cst) - 1;

			// if we tried to pop() form an empty queue
			assert(bottom >= -1);

			int64_t oldTop = top.load(std::memory_order_seq_cst);

			// Queue isn't empty and it's not the last item, just return it, this is the common case.
			if(oldTop < newBottom) {
				return items[newBottom & MASK];
			}

			T item{};
			if(oldTop == newBottom) {
				// last item in the queue
				item = items[newBottom & MASK];

				// we've just took the last item. It may cause data-race with steal()
				// We resolve this potential race by also stealing that item from ourselves.
				if(top.compare_exchange_strong(oldTop, oldTop + 1, std::memory_order_seq_cst, std::memory_order_relaxed)) {
					// the main thread of this queue has taken this item
					// adjust top to make the queue empty
					oldTop++;
				} else {
					// a different thread has stole the item so we discard this item
					item = T{};
				}
			} else {
				// empty queue
				assert(oldTop - newBottom == 1);
			}

			bottom.store(oldTop, std::memory_order_relaxed);
			return item;
		}

		[[nodiscard]] inline T steal()
		{
			while(true) {
				// A Key component of this algorithm is that mTop is read before mBottom
				int64_t oldTop = top.load(std::memory_order_seq_cst);
				int64_t oldBottom = bottom.load(std::memory_order_seq_cst);

				if(oldTop >= oldBottom) {
					// queue is empty
					return T{};
				}

				T item{ items[oldTop & MASK] };
				if(top.compare_exchange_strong(oldTop, oldTop + 1, std::memory_order_seq_cst, std::memory_order_relaxed)) {
					return item;
				}

				//the item we've just tried to steal was pop()'ed, continue the loop, try steal something else
			}
		}

		[[nodiscard]] inline size_t max_size() const
		{
			return SIZE;
		}

		[[nodiscard]] inline size_t size() const
		{
			int64_t b = bottom.load(std::memory_order_relaxed);
			int64_t t = top.load(std::memory_order_relaxed);
			return b - t;
		}

		[[nodiscard]] inline bool empty() const
		{
			int64_t b = bottom.load(std::memory_order_relaxed);
			int64_t t = top.load(std::memory_order_relaxed);
			return  b <= t;
		}

		[[nodiscard]] inline T& operator=(size_t index) { return items[index & MASK]; }
		[[nodiscard]] inline const T& operator=(size_t index) const { return items[index & MASK]; }

	private:
		std::array<T, SIZE> items{};

		std::atomic_int64_t top{ 0 };
		std::atomic_int64_t bottom{ 0 };
	};
}