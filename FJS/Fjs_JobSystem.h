#pragma once

#include <condition_variable>
#include <thread>
#include <functional>
#include <random>
#include <unordered_map>

#include "Fjs_ObjectPool.h"
#include "Fjs_WorkStealingQueue.h"
#include "Fjs_TicketMutex.h"

namespace Fjs {
	//0x7FFE = 32766
	//32766 = std::numeric_limits<uint16_t>::max() / 2 - 1;
	static constexpr size_t INVALID_JOB_INDEX = std::numeric_limits<uint16_t>::max() / 2 - 1;
	static constexpr size_t CACHELINE_SIZE = std::hardware_destructive_interference_size;
	static constexpr size_t MAX_JOB_COUNT = std::numeric_limits<uint16_t>::max() / 4 + 1; //16384
	static_assert(MAX_JOB_COUNT <= INVALID_JOB_INDEX, "MAX_JOB_COUNT must be <= 0x7FFE");

	using WorkQueue = WorkStealingQueue<uint16_t, MAX_JOB_COUNT>;

	struct JobSystem;
	struct alignas(CACHELINE_SIZE) Job
	{
		using Task = void(*)(void*, JobSystem&, Job*);

		Job() = default;
		~Job() = default;

		Job(const Job&) = delete;
		Job(Job&&) = delete;

		inline void Run(JobSystem& jobSystem)
		{
			task(storage, jobSystem, this);
		}

		[[nodiscard]] inline bool isFinished() const
		{
			return unfinishedJobs <= 0;
		}

	private:
		friend struct JobSystem;

		static constexpr size_t JOB_STORAGE_SIZE_BYTES = sizeof(std::function<void()>) > 48 ? sizeof(std::function<void()>) : 48;
		static constexpr size_t JOB_STORAGE_SIZE_WORDS = (JOB_STORAGE_SIZE_BYTES + sizeof(void*) - 1) / sizeof(void*);

		void* storage[JOB_STORAGE_SIZE_WORDS];
		Task task;
		uint16_t parent = 0;
		std::atomic<uint16_t> unfinishedJobs = { 1 };
		mutable std::atomic<uint16_t> refCount = { 1 };
	};

	struct alignas(CACHELINE_SIZE) Worker
	{
		WorkQueue workQueue;
		std::thread thread;
		JobSystem* js = nullptr;
		std::random_device rndGen;
	};

	struct JobSystem
    {
        JobSystem(size_t threadCount = 0, size_t pushableThreadsCount = 1);
        ~JobSystem();

		[[nodiscard]] Job* CreateJob(Job::Task task, Job* parent = nullptr);

		//the caller must ensure the object will outlive the Job
		template<typename T, void(T::*method)(JobSystem&, Job*)>
		[[nodiscard]] inline Job* CreateJob(T* data, Job* parent = nullptr)
		{
			Job* job = CreateJob([](void* user, JobSystem& js, Job* job) {
				(*static_cast<T**>(user)->*method)(js, job);
			}, parent);
			if (job) {
				job->storage[0] = data;
			}
			return job;	
		}

		//the caller must ensure the object will outlive the Job
		template<typename T, void(T::*method)(JobSystem&, Job*)>
		[[nodiscard]] inline Job* CreateJob(T data, Job* parent = nullptr)
		{
			static_assert(sizeof(data) <= sizeof(Job::storage), "user data too large");
			Job* job = CreateJob([](void* user, JobSystem& js, Job* job) {
				T* that = static_cast<T*>(user);
				(that->*method)(js, job);
				that->~T();
			}, parent);
			if (job) {
				new(job->storage) T(std::move(data));
			}
			return job;
		}

		[[nodiscard]] inline Job* CreateJobFunctor(std::invocable<JobSystem&, Job*> auto&& functor, Job* parent)
		{
			static_assert(sizeof(functor) <= sizeof(Job::storage), "functor too large");
			Job* job = CreateJob([](void* user, JobSystem& js, Job* job) {
				auto& func = *static_cast<decltype(functor)*>(user);
				func(js, job);
				func.~T();
			}, parent);

			if(job) {
				new(job->storage) decltype(functor)(std::move(functor));
			}
			return job;
		}

		//unfortunately parent cannot be the last parameter, because of variadic template arguments
		//to be consistent with the API I could change every CreateJob() function to have parent as the first parameter, but I do not like it.
		template<typename ... Args>
		[[nodiscard]] inline Job* CreateJob(Job* parent, std::invocable<Args...> auto&& func, Args&&... args)
		{
			struct Data
			{
				std::function<void()> f;
				inline void Execute(JobSystem&, Job*) { f(); }
			} user{ std::bind(std::forward<decltype(func)>(func), std::forward<Args>(args)...)};
			return CreateJob<Data, &Data::Execute>(std::move(user), parent);
		}

		void Run(Job*& job);
		inline void Run(Job*&& job)
		{
			Job* p = job;
			Run(p);
		}

		//runs a job and hold its ref count untill it gets release
		[[nodiscard]] Job* RunAndRetain(Job* job);
		
		//waits for a job and releases its ref count
		void WaitAndRelease(Job*& job);

		void RunAndWait(Job*& job);
		inline void RunAndWait(Job*&& job)
		{
			Job* p = job;
			RunAndWait(p);
		}

		//increases a job's ref count and returns the job
		[[nodiscard]] Job* Retain(Job* job);

		//decreases a job's ref count and invalidates it
		void Release(Job*& job);
		inline void Release(Job*&& job)
		{
			Job* p = job;
			Release(p);
		}

		//wakes up all threads
		void Signal();
		void Cancel(Job*& job);

		[[nodiscard]] inline Job* setRootJob(Job* job)
		{
			return rootJob = job;
		}

		//pushes currently used thread to the job system's thread pool
		void PushCurrentThread();
		
		//pops currently used thread from the job system's thread pool
		void PopCurentThread();

		[[nodiscard]] inline size_t getWorkersCount() const { return workers.size(); }

    private:
		friend struct Job;

        Worker& getWorker();

        void AddRef(Job const* job);
		//decreases ref count AND destroys the job if it's needed
        void RemRef(Job* job);

		[[nodiscard]] Worker* getWorkerToStealFrom(Worker& worker);

        void Quit();
        [[nodiscard]] bool QuitRequested() const;
        [[nodiscard]] bool hasActiveJobs() const;

		[[nodiscard]] bool Execute(Worker& worker);

		[[nodiscard]] Job* Steal(Worker& worker);
        void Finish(Job* job);

        inline void Push(WorkQueue& workQueue, Job* job)
        {
            assert(job);
            size_t index = job - jobAddress;
            assert(index >= 0 && index < MAX_JOB_COUNT);
            workQueue.push(uint16_t(index + 1));
        }

		[[nodiscard]] inline Job* Pop(WorkQueue& workQueue)
        {
            size_t index = workQueue.pop();
            assert(index <= MAX_JOB_COUNT);
            return !index ? nullptr : &jobAddress[index - 1];
        }

		[[nodiscard]] inline Job* Steal(WorkQueue& workQueue)
        {
            size_t index = workQueue.steal();
            assert(index <= MAX_JOB_COUNT);
            return !index ? nullptr : &jobAddress[index - 1];
        }

        void Wait(std::unique_lock<TicketMutex>& lock, Job* job = nullptr);
        void WakeAll();
        void WakeOne();

		TicketMutex threadMapMutex;
		std::unordered_map<std::thread::id, Worker*> threadMap;

		TicketMutex waiterMutex;
		std::condition_variable_any waiterCv;

		alignas(CACHELINE_SIZE / 4) std::vector<Worker> workers;
		ObjectPool<Job, MAX_JOB_COUNT> jobPool;

		Job* rootJob = nullptr;
		//used to get parent index and to access jobs from the job pool
        Job* jobAddress = nullptr;  

		std::atomic<uint32_t> activeJobs = { 0 };

		std::atomic<uint16_t> pushedThreads = { 0 };
        uint16_t threadsCount = 0;    

		std::atomic<bool> quit = { false };
    };	
}