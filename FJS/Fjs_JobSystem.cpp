#include "Fjs_JobSystem.h"

namespace Fjs {
    JobSystem::JobSystem(const size_t userThreadCount, const size_t pushableThreadsCount)
    {
        jobAddress = jobPool.first();

        size_t threadPoolCount = userThreadCount;
        if(threadPoolCount == 0) {
            threadPoolCount = std::thread::hardware_concurrency() - 1;
        }

        threadPoolCount = std::max(1, (int)threadPoolCount);
        threadPoolCount = std::min(32, (int)threadPoolCount);

        workers = std::vector<Worker>(threadPoolCount + pushableThreadsCount);
        threadsCount = uint16_t(threadPoolCount);

        static_assert(std::atomic<bool>::is_always_lock_free);
        static_assert(std::atomic<uint16_t>::is_always_lock_free);

        const size_t hardwareThreadCount = threadsCount;

        for(size_t i = 0, n = workers.size(); i < n; i++) {
            auto& worker = workers[i];
            worker.js = this;
            if(i < hardwareThreadCount) {
                worker.thread = std::thread([](Worker& worker, JobSystem& js) {
                    {
                        std::scoped_lock guard(js.threadMapMutex);
                        bool inserted = js.threadMap.emplace(std::this_thread::get_id(), &worker).second;
                        assert(inserted, "This thread is already in a loop.");
                    }

                    while(!js.QuitRequested()) {
                        if(!js.Execute(worker)) {
                            std::unique_lock lock(js.waiterMutex);
                            while(!js.QuitRequested() && !js.hasActiveJobs()) {
                                js.Wait(lock);
                            }
                        }
                    }
                }, std::ref(worker), std::ref(*this));
            }
        }
    }

    JobSystem::~JobSystem()
    {
        Quit();

        for(auto& worker : workers) {
            if(worker.thread.joinable()) {
                worker.thread.join();
            }
        }
    }

    Job* JobSystem::CreateJob(Job::Task func, Job* parent)
    {
        parent = (parent == nullptr) ? rootJob : parent;
        Job* const job = jobPool.create_object();
        if(job) {
            size_t index = INVALID_JOB_INDEX;
            if(parent) {
                const uint16_t parentJobCount = parent->unfinishedJobs.fetch_add(1, std::memory_order_relaxed);

                assert(parentJobCount > 0);

                index = parent - jobAddress;
                assert(index < MAX_JOB_COUNT);
            }
            job->task = func;
            job->parent = uint16_t(index);
        }
        return job;
    }

    void JobSystem::Run(Job*& job)
    {
        Worker& worker = getWorker();

        activeJobs.fetch_add(1, std::memory_order_relaxed);

        Push(worker.workQueue, job);

        WakeOne();

        job = nullptr;
    }

    Job* JobSystem::RunAndRetain(Job* job)
    {
        Job* retained = Retain(job);
        Run(job);
        return retained;
    }

    void JobSystem::WaitAndRelease(Job*& job)
    {
        assert(job);
        assert(job->refCount.load(std::memory_order_relaxed) >= 1);

        Worker& worker = getWorker();
        while(!job->isFinished() && !QuitRequested()) {
            if(!Execute(worker)) {
                //if this job is finished, just break the loop and avoid the lock
                if(job->isFinished()) {
                    break;
                }

                //if it's not finished, but 
                std::unique_lock lock(waiterMutex);
                if(!job->isFinished() && !hasActiveJobs() && !QuitRequested()) {
                    Wait(lock, job);
                }
            }
        }

        if(job == rootJob) {
            rootJob = nullptr;
        }

        Release(job);
    }

    void JobSystem::RunAndWait(Job*& job)
    {
        RunAndRetain(job);
        WaitAndRelease(job);
    }

    Job* JobSystem::Retain(Job* job)
    {
        Job* retained = job;
        AddRef(retained);
        return retained;
    }

    void JobSystem::Release(Job*& job)
    {
        RemRef(job);
        job = nullptr;
    }

    void JobSystem::Signal()
    {
        WakeAll();
    }

    void JobSystem::Cancel(Job*& job)
    {
        Finish(job);
        job = nullptr;
    }

    inline void JobSystem::AddRef(Job const* job)
    {
        job->refCount.fetch_add(1, std::memory_order_relaxed);
    }

    void JobSystem::RemRef(Job* job)
    {
        const uint16_t c = job->refCount.fetch_sub(1, std::memory_order_acq_rel);
        assert(c > 0);
        //check if c is 1, because fetch_sub does return the old value, so the actual refCount is 0. 
        if(c == 1) {
            jobPool.destroy_object(job);
        }
    }

    void JobSystem::Quit()
    {
        quit.store(true);
        std::scoped_lock lock(waiterMutex);
        waiterCv.notify_all();
    }

    bool JobSystem::QuitRequested() const
    {
        return quit.load(std::memory_order_relaxed);
    }

    bool JobSystem::hasActiveJobs() const
    {
        return activeJobs.load(std::memory_order_relaxed) > 0;
    }

    void JobSystem::Wait(std::unique_lock<TicketMutex>& lock, Job* job)
    {
        static constexpr bool DEBUG_FINISH_HANGS = false;
        if constexpr(!DEBUG_FINISH_HANGS) {
            waiterCv.wait(lock);
        } else {
            do {
                const std::cv_status status = waiterCv.wait_for(lock, std::chrono::milliseconds(4000));
                if(status == std::cv_status::no_timeout) {
                    break;
                }
            } while(true);
        }
    }

    void JobSystem::WakeAll()
    {
        std::scoped_lock lock(waiterMutex);
        waiterCv.notify_all();
    }

    void JobSystem::WakeOne()
    {
        std::scoped_lock lock(waiterMutex);
        waiterCv.notify_one();
    }

    void JobSystem::PushCurrentThread()
    {
        const auto id = std::this_thread::get_id();

        std::unique_lock lock(threadMapMutex);
        const auto iter = threadMap.find(id);
        const Worker* const worker = iter == threadMap.end() ? nullptr : iter->second;
        lock.unlock();

        if(worker) {
            //assert(this == state->js,
            //					"Called push on a thread owned by another JobSystem (%p), this=%p!",
            //					state->js, this);
            return;
        }

        const uint16_t pushable = pushedThreads.fetch_add(1, std::memory_order_relaxed);
        const size_t index = threadsCount + pushable;

        assert(index < workers.size(), "Too many calls to PushCurrentThread(). No more pushable threads!");

        lock.lock();
        threadMap[id] = &workers[index];
    }

    void JobSystem::PopCurentThread()
    {
        const auto id = std::this_thread::get_id();
        std::scoped_lock lock(threadMapMutex);
        const auto iter = threadMap.find(id);
        const Worker* const worker = iter == threadMap.end() ? nullptr : iter->second;
        assert(worker, "this thread is not an pushed thread");
        assert(worker->js == this, "this thread is not pushed by us");
        threadMap.erase(iter);
    }

    Worker& JobSystem::getWorker()
    {
        std::scoped_lock lock(threadMapMutex);
        const auto iter = threadMap.find(std::this_thread::get_id());
        assert(iter != threadMap.end(), "This thread has not been pushed.");
        return *iter->second;
    }

    Worker* JobSystem::getWorkerToStealFrom(Worker& worker)
    {
        const uint16_t pushable = pushedThreads.load(std::memory_order_relaxed);
        const uint16_t threadCount = threadsCount + pushable;

        Worker* workerToStealFrom = nullptr;

        if(threadCount >= 2) {
            do {
                const std::uniform_int_distribution<uint16_t> distribution(0, static_cast<uint16_t>(workers.size()) - 1);
                const uint16_t index = distribution(worker.rndGen);
                assert(index < workers.size());
                workerToStealFrom = &workers[index];
            } while(workerToStealFrom == &worker);
        }
        return workerToStealFrom;
    }

    Job* JobSystem::Steal(Worker& worker)
    {
        Job* job = nullptr;
        while(!job && hasActiveJobs()) {
            Worker* const workerToStealFrom = getWorkerToStealFrom(worker);
            if(workerToStealFrom) {
                job = Steal(workerToStealFrom->workQueue);
            }
        }
        return job;
    }

    bool JobSystem::Execute(Worker& worker)
    {
        Job* job = Pop(worker.workQueue);
        if(job == nullptr) {
            // our queue is empty, try to steal a job
            job = Steal(worker);
        }

        if(job) {
            assert(job->unfinishedJobs.load(std::memory_order_relaxed) >= 1);

            const uint32_t aj = activeJobs.fetch_sub(1, std::memory_order_relaxed);
            assert(aj); // we were already at 0

            job->Run(*this);
            Finish(job);
        }
        return job != nullptr;
    }

    void JobSystem::Finish(Job* job)
    {
        bool notify = false;

        Job* const storage = jobAddress;
        while(job) {
            uint16_t runningJobCount = job->unfinishedJobs.fetch_sub(1, std::memory_order_acq_rel);
            assert(runningJobCount > 0);
            if(runningJobCount == 1) {
                notify = true;
                Job* const parent = job->parent == INVALID_JOB_INDEX ? nullptr : &storage[job->parent];
                RemRef(job);
                job = parent;
            } else break;
        }

        if(notify) {
            WakeAll();
        }
    }
}