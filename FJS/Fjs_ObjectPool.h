#pragma once

#include <memory>
#include <atomic>
#include <exception>

namespace Fjs {
    template<typename T, size_t SIZE>
    struct ObjectPool
    {
        ObjectPool()
        {
            for(size_t i = 1; i < SIZE; ++i) {
                pool[i - 1].next = &pool[i];
            }

            nextFree = &pool[0];
        }

        ~ObjectPool() = default;

        ObjectPool(const ObjectPool&) = delete;
        ObjectPool& operator =(const ObjectPool&) = delete;

        ObjectPool(ObjectPool&& other)
            : pool{ std::move(other.pool) }, nextFree{ other.nextFree }
        {
            other.nextFree = nullptr;
        }

        ObjectPool& operator =(ObjectPool&& other)
        {
            if(this == &other) {
                return *this;
            }

            pool = std::move(other.pool);
            nextFree = other.nextFree;

            other.nextFree = nullptr;

            return *this;
        }

        template<typename U = T, typename ... Args>
        [[nodiscard]] inline T* create_object(Args&& ... args)
        {
            T* mem = allocate();
            construct<U>(mem, std::forward<Args>(args)...);
            return mem;
        }

        template<typename U>
        inline void destroy_object(U* mem)
        {
            destroy(mem);
            deallocate(mem);
        }

        [[nodiscard]] inline T* allocate()
        {
            if (!nextFree) throw std::exception{"allocate(): nextFree is nullptr"};

            auto item = nextFree.load(std::memory_order_relaxed);

            while(item != nullptr && !nextFree.compare_exchange_weak(item, item->next));

            ++m_size;

            return reinterpret_cast<T*>(&item->storage);
        }

        inline void deallocate(T* p)
        {
            if (!p) throw std::exception{"deallocate(): p is nullptr"};
            const auto item = reinterpret_cast<Item*>(p);

            item->next = nextFree;
            while(!nextFree.compare_exchange_weak(item->next, item));

            --m_size;

            if (!nextFree) throw std::exception{"deallocate(): nextFree is nullptr"};
        }

        template<typename U, typename ... Args>
        inline void construct(U* mem, Args&& ...args)
        {
            new (mem) U(std::forward<Args>(args)...);
        }

        template<typename U>
        inline void destroy(U* mem)
        {
            if(!mem) return;

            mem->~U();
        }

        [[nodiscard]] inline T* first() const
        {
            return reinterpret_cast<T*>(&pool[0].storage);
        }

        [[nodiscard]] inline T* last() const
        {
             return reinterpret_cast<T*>(&pool[SIZE - 1].storage);
        }

        [[nodiscard]] inline T* operator[](size_t i) const
        {
            return reinterpret_cast<T*>(&pool[i].storage);
        }

        [[nodiscard]] inline uint32_t size() const
        {
            return m_size.load(std::memory_order_relaxed);
        }

        [[nodiscard]] inline uint32_t max_size() const
        {
            return SIZE;
        }
    private:
        union Item
        {
            alignas(alignof(T)) uint8_t storage[sizeof(T)];
            Item* next;
        };

        std::unique_ptr<Item[]> pool = std::make_unique<Item[]>(SIZE);
        std::atomic<Item*> nextFree{ nullptr };

        std::atomic_uint32_t m_size{ 0 };
    };
}