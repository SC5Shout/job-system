workspace "FJS"
    architecture "x64"
    targetdir "build"

    configurations {
        "Debug",
        "Release"
    }

    flags {       
        "MultiProcessorCompile"
    }

    startproject "FJS"

    outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

    include "FJS"
    include "Tests"
