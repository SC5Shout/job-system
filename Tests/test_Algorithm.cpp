#include <gtest/gtest.h>
#include <Fjs_Algorithm.h>

TEST(Algorithm, ForEachIntegral)
{
	Fjs::JobSystem js{ 4 };
	js.PushCurrentThread();

    const uint32_t n = 9;

    uint32_t a = 0, b = 1, c, i;
    if (n != 0) {
        //n + 1, because the algorithm includes the 9th fib number, but the for_each function loops to less than, not less than or equal.
        Fjs::for_each(js, 2U, n + 1, [&a, &b, &c](uint32_t i) {
            c = a + b;
            a = b;
            b = c;
        });
    }

    EXPECT_EQ(b, 34);
}

TEST(Algorithm, ForEach)
{
    Fjs::JobSystem js{ 4 };
    js.PushCurrentThread();

    std::vector<int> values{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    int result = 0;
    
    Fjs::for_each(js, values, [&result](int i) {
        result += i;
    });

    EXPECT_EQ(result, 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10);
}