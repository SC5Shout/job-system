#include <gtest/gtest.h>
#include <Fjs_JobSystem.h>

TEST(JobSystem, SimpleJobs)
{
    Fjs::JobSystem js{4};
    js.PushCurrentThread();
    
    struct SimpleJob
    {
        std::atomic_int counter = 0;
        void func(Fjs::JobSystem& js, Fjs::Job* parent)
        {
            ++counter;
        }
    };
    
    SimpleJob data;
    std::array<Fjs::Job*, 10> jobs;
    for (int i = 0; i < 10; ++i) {
        auto job = js.CreateJob<SimpleJob, &SimpleJob::func>(&data, nullptr);
        js.RunAndRetain(job);
        jobs[i] = job;
    }
    
    for (auto job : jobs) {
        js.WaitAndRelease(job);
    }
    
    EXPECT_EQ(data.counter, 10);
}

TEST(JobSystem, SeqParentJobs)
{
    Fjs::JobSystem js{ 4 };
    js.PushCurrentThread();

    struct Fibonacci
    {
        int c = 0;
        int i = 0;
        int j = 0;
        void func(Fjs::JobSystem& js, Fjs::Job* parent)
        {
            if (c < 43) {
                Fibonacci u{ c + 1 };
                Fjs::Job* p = js.CreateJob<Fibonacci, &Fibonacci::func>(&u, parent);
                js.RunAndWait(p);
                i = u.i + u.j;
                j = u.i;
            } else {
                i = 0;
                j = 1;
            }
        }
    };

    Fibonacci fib{0};
    js.RunAndWait(js.CreateJob<Fibonacci, &Fibonacci::func>(&fib, nullptr));

    EXPECT_EQ(fib.i, 433494437);
}