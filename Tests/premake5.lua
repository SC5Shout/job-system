include "googletest"

project "Tests"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++20"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-obj/" .. outputdir .. "/%{prj.name}")

    files { 
        "**.h", 
        "**.c", 
        "**.hpp", 
        "**.cpp",
    }

    includedirs {
        "/",
        "%{wks.location}/Tests/googletest/googletest/include",
        "%{wks.location}/Tests/googletest/googlemock/include",
        "%{wks.location}/FJS"
    }

    links {
        "GoogleTest",
        "FJS"
    }

    filter "system:windows"
    systemversion "latest"

    filter "configurations:Debug"
        symbols "On"

    filter "configurations:Release"
        optimize "On"