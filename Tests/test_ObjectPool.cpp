#include <gtest/gtest.h>
#include <Fjs_ObjectPool.h>

#include <queue>

TEST(ObjectPool, AllocateDeallocate_ST)
{
	Fjs::ObjectPool<int, 1024> pool;

	for (int i = 0; i < pool.max_size(); ++i) {
		int* f = pool.allocate();
		EXPECT_EQ(pool[0], f);

		pool.deallocate(f);
	}
	EXPECT_EQ(pool.size(), 0);

	std::vector<int*> ptrs(pool.max_size());
	for (int i = 0; i < pool.max_size(); ++i) {
		ptrs[i] = pool.allocate();
		EXPECT_EQ(pool[i], ptrs[i]);
	}

	EXPECT_EQ(pool.size(), pool.max_size());

	for (int* ptr : ptrs) {
		pool.deallocate(ptr);
	}

	EXPECT_EQ(pool.size(), 0);
}

TEST(ObjectPool, AllocateDeallocate_MT)
{
	Fjs::ObjectPool<int, 1024> pool;

	{
		int c = pool.max_size() / 4;

		std::thread t([&]() {
			for (int i = 0; i < c; ++i) {
				pool.deallocate(pool.allocate());
			}
		});

		std::thread t2([&]() {
			for (int i = 0; i < c; ++i) {
				pool.deallocate(pool.allocate());
			}
		});

		std::thread t3([&]() {
			for (int i = 0; i < c; ++i) {
				pool.deallocate(pool.allocate());
			}
		});

		std::thread t4([&]() {
			for (int i = 0; i < c; ++i) {
				pool.deallocate(pool.allocate());
			}
		});

		t.join();
		t2.join();
		t3.join();
		t4.join();

		EXPECT_EQ(pool.size(), 0);
	}

	{
		int c = pool.max_size() / 2;

		std::queue<int*> ptrs;
		std::mutex m;
		std::thread t([&]() {
			for (int i = 0; i < c; ++i) {
				int* ptr = pool.allocate();
				{
					std::lock_guard g(m);
					ptrs.push(ptr);
				}
			}
		});

		std::thread t2([&]() {
			for (int i = 0; i < c; ++i) {
				int* ptr = pool.allocate();
				{
					std::lock_guard g(m);
					ptrs.push(ptr);
				}
			}
		});

		t.join();
		t2.join();

		EXPECT_EQ(pool.size(), pool.max_size());

		std::thread t3([&]() {
			for (int i = 0; i < c; ++i) {
				int* ptr = nullptr;
				{
					std::lock_guard g(m);
					ptr = ptrs.front();
					ptrs.pop();
				}

				pool.deallocate(ptr);
			}
		});

		std::thread t4([&]() {
			for (int i = 0; i < c; ++i) {
				int* ptr = nullptr;
				{
					std::lock_guard g(m);
					ptr = ptrs.front();
					ptrs.pop();
				}

				pool.deallocate(ptr);
			}
		});

		t3.join();
		t4.join();

		EXPECT_EQ(pool.size(), 0);
	}
}