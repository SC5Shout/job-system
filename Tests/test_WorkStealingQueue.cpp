#include <gtest/gtest.h>
#include <Fjs_WorkStealingQueue.h>

TEST(WorkStealingQueue, WorkStealingQueue_ST_PushPop)
{
    Fjs::WorkStealingQueue<int, 4096> queue;

    //simple push/pop
    int i = 5;
    queue.push(i);
    EXPECT_EQ(i, queue.pop());

    //many push/pop
    for (int i = 0; i < queue.max_size(); ++i) {
        queue.push(i);
    }
    EXPECT_EQ(queue.max_size(), queue.size());

    for (int i = 0; i < queue.max_size(); ++i) {
        int result = queue.pop();
        EXPECT_EQ(4095 - i, result);
    }
    EXPECT_EQ(0, queue.size());
}

TEST(WorkStealingQueue, WorkStealingQueue_ST_PushSteal)
{
    Fjs::WorkStealingQueue<int, 4096> queue;

    //simple push/steal
    int i = 5;
    queue.push(i);
    EXPECT_EQ(i, queue.steal());

    //many push/steal
    for (int i = 0; i < queue.max_size(); ++i) {
        queue.push(i);
    }
    EXPECT_EQ(queue.max_size(), queue.size());

    for (int i = 0; i < queue.max_size(); ++i) {
        int result = queue.steal();
        EXPECT_EQ(i, result);
    }
    EXPECT_EQ(0, queue.size());
}

TEST(WorkStealingQueue, WorkStealingQueue_MT_PopSteal)
{
    struct MyJob {};
    Fjs::WorkStealingQueue<MyJob*, 4096> queue;
    size_t size = queue.max_size();

    MyJob popJob;
    MyJob stealJob;

    for (size_t i = 0 ; i < size / 2 ; i++) {
        queue.push(&stealJob);
    }
    for (size_t i = 0 ; i < size / 2 ; i++) {
        queue.push(&popJob);
    }

    size_t pop_size = size / 2;
    std::thread pop_thread([&]() {
        for (int i=0 ; i<pop_size ; i++) {
            EXPECT_EQ(&popJob, queue.pop());
        }
    });

    size_t steal_size = size / (2*4);
    std::thread steal_thread0([&]() {
        for (int i=0 ; i<steal_size ; i++) {
            EXPECT_EQ(&stealJob, queue.steal());
        }
    });
    std::thread steal_thread1([&]() {
        for (int i=0 ; i<steal_size ; i++) {
            EXPECT_EQ(&stealJob, queue.steal());
        }
    });
    std::thread steal_thread2([&]() {
        for (int i=0 ; i<steal_size ; i++) {
            EXPECT_EQ(&stealJob, queue.steal());
        }
    });
    std::thread steal_thread3([&]() {
        for (int i=0 ; i<steal_size ; i++) {
            EXPECT_EQ(&stealJob, queue.steal());
        }
    });

    steal_thread0.join();
    steal_thread1.join();
    steal_thread2.join();
    steal_thread3.join();
    pop_thread.join();

    EXPECT_EQ(0, queue.size());
}

TEST(WorkStealingQueue, WorkStealingQueue_MT_PushPopSteal)
{
    struct TestJob {};
    Fjs::WorkStealingQueue<TestJob*, 4096> queue;
    TestJob job;

    int pop = 0;
    int steal0 = 0;
    int steal1 = 0;
    int steal2 = 0;
    int steal3 = 0;

    size_t push_size = queue.max_size() / 4;
    std::thread t1([&]() {
        for (int i = 0; i < push_size; i++) {
            queue.push(&job);
            queue.push(&job);
            queue.push(&job);
            queue.push(&job);

            if (&job == queue.pop())
                pop++;
            if (&job == queue.pop())
                pop++;
            if (&job == queue.pop())
                pop++;
            if (&job == queue.pop())
                pop++;
        }
    });

    size_t steal_size = queue.max_size();
    std::thread t2([&]() {
        for (int i = 0; i < steal_size; i++) {
            if (&job == queue.steal())
                steal0++;
        }
    });
    std::thread t3([&]() {
        for (int i = 0; i < steal_size; i++) {
            if (&job == queue.steal())
                steal1++;
        }
    });
    std::thread t4([&]() {
        for (int i = 0; i < steal_size; i++) {
            if (&job == queue.steal())
                steal2++;
        }
    });
    std::thread t5([&]() {
        for (int i = 0; i < steal_size; i++) {
            if (&job == queue.steal())
                steal3++;
        }
    });

    t5.join();
    t4.join();
    t3.join();
    t2.join();
    t1.join();

    EXPECT_EQ(pop + steal0 + steal1 + steal2 + steal3, queue.max_size());
    EXPECT_EQ(0, queue.size());
}