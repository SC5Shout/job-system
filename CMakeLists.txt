cmake_minimum_required(VERSION 3.19)

project ("FJS")

set(CMAKE_CXX_STANDARD 20)
set(WORK_LOCATION ${CMAKE_CURRENT_SOURCE_DIR})

# Include sub-projects.
add_subdirectory ("FJS")
add_subdirectory ("Tests")